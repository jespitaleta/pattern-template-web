import { useState, useEffect } from 'react';
import ReactLoading from 'react-loading';
import Sidebar from '/components/Sidebar';
import Catalog from '/components/Catalog';

export default function Home() {
  const sortByPrice = (a, b) => {
    if (a.price < b.price) {
      return -1;
    }
    if (a.price > b.price) {
      return 1;
    }
    return 0;
  };
  const sortByName = (a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  };

  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedElement, setSelectedElement] = useState(0);

  const fetchData = async () => {
    const response = await fetch('/api/products');
    const data = await response.json();
    setArticles(data);
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (loading) {
    return <div></div>;
  }

  return (
    <section className='bg-slate-100'>
      <div className='container px-6 py-8 mx-auto'>
        <div className='lg:flex lg:-mx-2'>
          <div className='space-y-3 lg:w-1/5 lg:px-2 lg:space-y-4'>
            <Sidebar
              articles={articles}
              selectedElement={selectedElement}
              selectionCallback={setSelectedElement}
            />
          </div>
          <Catalog articles={articles} selectedElement={selectedElement} />
        </div>
      </div>
    </section>
  );
}
