const articles = [
  {
    title: 'Pattern Making',
    products: [
      { name: 'Tops', price: '$1000', stock: true },
      { name: 'Shorts', price: '$1000', stock: true },
      { name: 'Pants', price: '$1000', stock: true },
      { name: 'Shirts', price: '$1000', stock: true },
      { name: 'Dress', price: '$1000', stock: true },
      { name: 'Skirt', price: '$1000', stock: true },
      { name: 'Bikini', price: '$1000', stock: true },
      { name: 'Jacket', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Digitizing',
    products: [
      { name: 'Tops Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Shorts Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Pants Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Shirts Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Dress Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Skirt Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Bikini Pattern Digitizing', price: '$1000', stock: true },
      { name: 'Jacket Pattern Digitizing', price: '$1000', stock: true },
    ],
  },
  {
    title: 'DXF File Converter',
    products: [
      { name: 'Convert DXF to Gerber', price: '$1000', stock: true },
      { name: 'Convert DXF to Illustrator', price: '$1000', stock: true },
      { name: 'Convert DXF to PDF', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Printing',
    products: [
      { name: 'Tops Pattern Printing', price: '$1000', stock: true },
      { name: 'Shorts Pattern Printing', price: '$1000', stock: true },
      { name: 'Pants Pattern Printing', price: '$1000', stock: true },
      { name: 'Shirts Pattern Printing', price: '$1000', stock: true },
      { name: 'Dress Pattern Printing', price: '$1000', stock: true },
      { name: 'Skirt Pattern Printing', price: '$1000', stock: true },
      { name: 'Bikini Pattern Printing', price: '$1000', stock: true },
      { name: 'Jacket Pattern Printing', price: '$1000', stock: true },
      { name: 'Nested Pattern Printing', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Resizing',
    products: [
      { name: 'Tops Pattern Grading', price: '$1000', stock: true },
      { name: 'Shorts Pattern Grading', price: '$1000', stock: true },
      { name: 'Pants Pattern Grading', price: '$1000', stock: true },
      { name: 'Shirts Pattern Grading', price: '$1000', stock: true },
      { name: 'Dress Pattern Grading', price: '$1000', stock: true },
      { name: 'Skirt Pattern Grading', price: '$1000', stock: true },
      { name: 'Bikini Pattern Grading', price: '$1000', stock: true },
      { name: 'Jacket Pattern Grading', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Marker Making',
    products: [
      { name: 'Tops Marker Making', price: '$1000', stock: true },
      { name: 'Shorts Marker Making', price: '$1000', stock: true },
      { name: 'Pants Marker Making', price: '$1000', stock: true },
      { name: 'Shirts Marker Making', price: '$1000', stock: true },
      { name: 'Dress Marker Making', price: '$1000', stock: true },
      { name: 'Skirt Marker Making', price: '$1000', stock: true },
      { name: 'Bikini Marker Making', price: '$1000', stock: true },
      { name: 'Jacket Marker Making', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Clothing Alterations',
    products: [
      { name: 'Tops Alterations', price: '$1000', stock: true },
      { name: 'Shorts Alterations', price: '$1000', stock: true },
      { name: 'Pants Alterations', price: '$1000', stock: true },
      { name: 'Shirts Alterations', price: '$1000', stock: true },
      { name: 'Dress Alterations', price: '$1000', stock: true },
      { name: 'Skirt Alterations', price: '$1000', stock: true },
      { name: 'Bikini Alterations', price: '$1000', stock: true },
      { name: 'Jacket Alterations', price: '$1000', stock: true },
      { name: 'Safety Gear Alterations', price: '$1000', stock: true },
      { name: 'Pillow / Wrap Altrations', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Fabric Shrinkage',
    products: [
      { name: 'Tops Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Shorts Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Pants Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Shirts Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Dress Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Skirt Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Bikini Fabric Shrinkage', price: '$1000', stock: true },
      { name: 'Jacket Fabric Shrinkage', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Women Sewing Patterns',
    products: [
      { name: 'Plus Size Jeans Boot Fit Combo Pattern', price: '$1000', stock: true },
      {
        name: 'Plus Size Jeans Straight Fit Combo Pattern',
        price: '$1000',
        stock: true,
      },
      { name: 'Plus Size Jeans Skinny Fit Combo Pattern', price: '$1000', stock: true },
      { name: 'Plus Size Jeans Crop Fit Combo Pattern', price: '$1000', stock: true },
      { name: 'Plus Size Jeans Slim Fit Combo Pattern', price: '$1000', stock: true },
      { name: 'Plus Size Jeans Shorts Combo Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Boot Fit Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Straight Fit Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Skinny Fit Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Crop Fit Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Slim Fit Pattern', price: '$1000', stock: true },
      { name: 'Misses Jeans Shorts Pattern', price: '$1000', stock: true },
    ],
  },
  {
    title: 'Men Sewing Patterns',
    products: [
      { name: 'Mens T-Shirt Round Neck Combo Pattern', price: '$1000', stock: true },
      { name: 'Mens T-Shirt V-Neck Combo Pattern', price: '$1000', stock: true },
      { name: 'Mens Long Sleeve Shirt Jacket Combo Pattern', price: '$1000', stock: true },
      { name: 'Mens Long Sleeve Fitted Shirt Combo Pattern', price: '$1000', stock: true },
      {
        name: 'Mens Long Sleeve Box Fit Shirt Combo Pattern',
        price: '$1000',
        stock: true,
      },
    ],
  },
  {
    title: 'Fabric Mask Patterns',
    products: [{ name: 'Face Mask PDF Pattern', price: '$1000', stock: true }],
  },
  {
    title: 'Scrub Caps Patterns',
    products: [{ name: 'Scrub Caps Classic PDF Pattern', price: '$1000', stock: true }],
  },
];

const sortByPrice = (a, b) => {
  if (a.price < b.price) {
    return -1;
  }
  if (a.price > b.price) {
    return 1;
  }
  return 0;
};
const sortByName = (a, b) => {
  if (a.name < b.name) {
    return -1;
  }
  if (a.name > b.name) {
    return 1;
  }
  return 0;
};

export default function handler(req, res) {
  res.status(200).json(articles);
}
