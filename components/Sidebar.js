import React from 'react';

const Sidebar = ({ articles, selectedElement, selectionCallback }) => {
  return (
    <div className='space-y-3 lg:space-y-4 bg-white p-5 rounded-2xl shadow-lg'>
      {articles.map((article, idx) => (
        <button
          href='#'
          key={idx}
          className={`block font-medium ${
            selectedElement == idx
              ? 'text-blue-600 bg-slate-50 rounded-lg px-5 py-2'
              : 'text-gray-600'
          } hover:underline`}
          onClick={() => selectionCallback(idx)}
        >
          {article.title}
        </button>
      ))}
    </div>
  );
};

export default Sidebar;
