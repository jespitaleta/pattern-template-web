import Header from './Header';
import Footer from './Footer';

const Layout = ({ children }) => {
  return (
    <div className=' w-full h-full min-h-screen'>
      <Header />
      <div className='top-20 bg-slate-500'>{children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
